Pkg.init()
Pkg.update()

Pkg.add("PyCall")
Pkg.build("PyCall")

Pkg.add("PyPlot")
Pkg.build("PyPlot")

Pkg.add("ZMQ")
Pkg.build("ZMQ")

Pkg.add("DSP")

Pkg.add("IJulia")
Pkg.build("IJulia")

Pkg.add("Interact")
Pkg.build("Interact")

Pkg.add("Reactive")
Pkg.build("Reactive")

Pkg.checkout("Interact")

Pkg.add("BenchmarkTools")
#Pkg.add("ProgressMeter")
#Pkg.add("Color")
#Pkg.add("Gadfly")
#Pkg.add("NumericExtensions")
#Pkg.add("Plots")
#Pkg.add("Winston")

using PyPlot
fig = figure("test");
plot(1:100,sin(1:100), "r");

